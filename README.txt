Description:
===========
Paranoia module is for all the sysadmins out there who think that
allowing random CMS admins to execute PHP of their choice is not
a safe idea.

What it does:
=============
- Disable the PHP module.
- Disable granting of the "use PHP for settings" permission.
  Save the permissions form once to remove all previous grants.
  (An error appears in the site status report if a role still has this
  permission.)
- Disable granting to Anonymous or Authenticated any permission that is
  marked "restrict access" in a module's hook_permission.
- Disable granting several permissions from popular contribs that are not
  marked as "restrict access" but are still important.
- Remove the PHP and paranoia modules from the module admin page.
- Provides a hook to let you remove other modules from the module admin page.

NOTE on uninstalling:
=====
The only way to uninstall the paranoia module is by changing its status in
the database config table. By design it does not show up in the module
uninstall administration page after it is enabled.

You can uninstall it with drush:
drush pm-uninstall paranoia

Or you can uninstall it by removing the module from the file system:
- Delete the module directory.
- Clear site caches or truncate the cache_config table.

Support
=======
View current issues:
http://drupal.org/project/issues/paranoia
Submit a new issue:
http://drupal.org/node/add/project-issue/paranoia

Development
===========
All development happens in branches like 8.x-1.x and 7.x-1.x.

Maintainers
======
Gerhard Killesreiter
Greg Knaddison @greggles
